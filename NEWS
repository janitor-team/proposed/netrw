$Id: NEWS,v 1.67 2006/08/15 20:32:39 jirka Exp $
------------------------------------------------------------------------------
Notation:
    + some new feature was added
    - something was removed
    * code change
    F a bug was fixed
    o note
------------------------------------------------------------------------------

version 1.3.2 (2006-08-15)
==========================
F  SIGSEGV when control connection is closed during initialization


version 1.3.1 (2006-03-03)
==========================
+  new examples using -i and -o options
F  netread may end up in an endless loop after receiving all data


version 1.3 (2006-02-06)
========================
*  prefer SHA-256 and SHA-1 algorithms to MD5
+  keep-alive packets for control connection to prevent stupid firewall/NAT
   machines from shutting down control connection of long data transfers
*  errors and warnings start with upper case letters
+  synchronous output for netread (option -s)
+  netwrite accepts -i <file> option for reading data from a file
+  netread accepts -o <file> option for writing data to a file
*  average speed for current line is printed at the end of each progress line
   instead of globally average speed
+  MacOS X testing platform
+  support for linking against diet libc (including its MD5 implementation)


version 1.2 (2004-07-19)
========================
+  README file
F  interoperability with the TCP/IP swiss army knife (netcat, nc)
*  better summary
+  speed can be printed in bits per second (option -b)
+  support for TiB, PiB, EiB ;)
*  better progress and speed printing
F  checksum help message looks like "none" is the default algorithm
+  FreeBSD 5.2 testing platform
*  small corrections in usage and version printing


version 1.1 (2003-04-23)
========================
+  verbose version information using -vV option
F  buggy built-in MD5 implementation was replaced by implementation from mhash
   library
+  --enable-static option for configure
F  one second delay before control socket creation
+  show transfer speed at the end of each progress line
*  minimum time is one second for speed computation
+  show checksum library within version information
F  link against libcrypto instead of libssl when using openssl library for
   checksum algorithms
F  use binary prefixes instead of decimal ones (GB => GiB)
F  dns resolver error messages


version 1.0.1 (2002-12-02)
==========================
*  successfully compiles on Windows (using cygwin of course ;o)
F  SIGSEGV when the control connection cannot be created


version 1.0 (2002-11-10)
========================
*  better netrw messages (esp. concerning checksum algorithm)
+  use transmission checksum by default (can be changed with
   ``--disable-default-checksum'' argument to configure)
*  option -c with optional argument was split into -c with no argument and
   -C with mandatory algorithm specification
*  successfully compiles on OSF (1.1) and OpenBSD (3.0)
F  use snprintf and vsnprintf wrappers when the original functions are not
   present
F  OpenBSD requires <netinet/in.h>


version 0.99.4 (2002-10-14)
===========================
o  the fourth prerelease of version 1.0
*  successfully compiles on IRIX (tested on 6.5) and Solaris (8)
F  cannot link without -lsocket -lnsl on Solaris
-  unused variables
F  some warnings concerning address size
F  socklen_t is undefined in net.h on SGI machines


version 0.99.3 (2002-10-14)
===========================
o  the third prerelease of version 1.0
F  cannot link with -lssl without -lcrypto on some systems
F  checksum usage (space between `-c' and `[algorithm]'
F  files getopt1.c, and md5.c are not distributed


version 0.99.2 (2002-10-14)
===========================
o  the second prerelease of version 1.0
+  TIGER and GOST mhash algorithm
F  OpenSSL checksum size
F  mhash checksum conversion
*  minor code cleanup
F  sha156 (misspelled sha256)


version 0.99.1 (2002-10-13)
===========================
o  the first prerelease of version 1.0
+  support for multiple checksum algorithms (using OpenSSL, mhash or built-in)
+  fallback getopt implementation (from GNU Libc)
+  THANKS file
*  CHANGES moved to NEWS
*  version info moved from common.h to version.h.in
F  ``service'' might be used uninitialized in netwrite.c and netread.c
*  all the source files moved to ``src'' directory
+  autoconf + automake build system
*  version info: show build time, built-in extensions, and copyright notice
-  socklen_t definition when undefined (doesn't work properly)
*  set SO_REUSEADDR on listening socket
F  error message about nonexistent CVS/Root
F  GCC 3.x compiler warning about label at the end of statement
F  netrw doesn't compile on SGI MIPS machines


version 0.10 (2002-08-05)
=========================
F  netwrite in firewall mode is not able to accept control connection
*  version info is passed through automaticaly generated version.h instead on
   CC command line
*  improved ``release'' target of the Makefile
*  version info: print release date and time with timezone
*  new version info format:
   "netrw tools version <ver> (<release date and time>)"
F  netwrite is calling useless bind() when using UDP protocol


version 0.9 (2002-08-04)
========================
+  ``release'' target to help me with creating release packages
+  GNU GPL preamble to each of the source files


version 0.8 (2002-08-04)
========================
*  mostly rewritten, common parts of code moved into separate files
+  TCP control connection with for data connection parameters negotiation
+  MD5 checksum support with automatic validation at the end of data
   transmission
+  static linking, ``help'' target, and some other Makefile improvements
F  OPT <=> OTP typo
F  struct hostent *host might be used uninitialized
+  optimizations and several Makefile improvements


version 0.7 (2002-07-22)
========================
F  netwrite shows usage instead of version with -V option


version 0.6 (2002-07-22)
========================
*  common options processing moved out from net{read,write}
+  hashes are printed showing a progress of data transfer


version 0.5 (2002-07-08)
========================
F  misspelled word ``connection''


version 0.4 (2002-06-15)
========================
F  warnings about passing pointers to incomplete data types


version 0.3 (2002-06-15)
========================
o  the first public release of netrw tools

