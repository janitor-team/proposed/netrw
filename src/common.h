/* $Id: common.h,v 1.5 2006/08/15 14:34:20 jirka Exp $ */

/*
 * netrw tools
 * Copyright (C) 2002 Jiri Denemark
 *
 * This file is part of netrw.
 *
 * netrw is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef COMMON_H
#define COMMON_H

#if HAVE_CONFIG_H
#include "config.h"
#endif

#if HAVE_UNSIGNED_LONG_LONG
#define UNSIGNED_LONG_LONG unsigned long long
#else
#define UNSIGNED_LONG_LONG unsigned long
#endif 

#if (SIZEOF_UNSIGNED_LONG_LONG >= 8 || SIZEOF_UNSIGNED_LONG >= 8)
#define HAVE_INT64  1
#else
#define HAVE_INT64  0
#endif

/* function return values */
#define FN_FAILURE  (-1)
#define FN_SUCCESS  ( 0)


#define max(x, y)   (((x) > (y)) ? (x) : (y))

#define KILO ((UNSIGNED_LONG_LONG)  1024)
#define MEGA ((UNSIGNED_LONG_LONG) (1024 * KILO))
#define GIGA ((UNSIGNED_LONG_LONG) (1024 * MEGA))
#if HAVE_INT64
#define TERA ((UNSIGNED_LONG_LONG) (1024 * GIGA))
#define PETA ((UNSIGNED_LONG_LONG) (1024 * TERA))
#define EXA  ((UNSIGNED_LONG_LONG) (1024 * PETA))
#endif

#define BUFFSIZE (1 << 14) /* 16 KB */


/* net{read,write} return values */
#define RETURN_VALUES \
    "return values\n"                                   \
    "  0                no errors.\n"                   \
    "  1                some error occured.\n"          \
    "  2                checksum validation failed.\n"  \

/* printing with verbosity */

#define QUIET       (-2)
#define ERROR       (-2)
#define WARNING     (-1)
#define NORMAL      ( 0)
#define VERBOSE     ( 1)
#define VVERBOSE    ( 2)
#define OPTION_q
#define OPTION_v
#define VERBOSITY_OPT "qv"
#define VERBOSITY_USAGE \
    "  -q               be quiet.\n"        \
    "  -v               be verbose.\n"      \
    "  -vv              be very verbose.\n"
#define VERBOSITY_CASE \
    case 'q':           /* quiet */     \
        set_verbosity(QUIET);           \
        break;                          \
    case 'v':           /* verbose */   \
        set_verbosity(VERBOSE);         \
        break;

/*
 * set_verbosity(v)
 *
 * set verbosity level to QUIET (for v == QUIET) or increase verbosity level
 * by one (for v == VERBOSE)
 *
 * return value:
 *      none
 *
 * side effects:
 *      static variable verbosity is affected
 */
extern void set_verbosity(int v);

/*
 * can_print(verbose_level)
 *
 * check when it is allowed to print a message whit the specified verbosity
 * level
 *
 * return value:
 *      zero ...... cannot print
 *      nonzero ... can print
 *
 * side effects:
 *      none
 */
extern int can_print(int verbose_level);

/*
 * print(verbose_level, msg, ...)
 *
 * print a message with the specified verbosity level
 * message is determined by format string msg and its parameters with
 * exactly the same meaning as for the *printf() functions
 *
 * return value:
 *      none
 *
 * side effects:
 *      none
 */
extern void print(int verbose_level, char *msg, ...);


/*****************************************************************************
 * string parsing
 */

/*
 * get_word(line)
 *
 * fetch the first word from the line, word are separated by spaces
 * the word is removed from the line
 *
 * return value:
 *      word 
 *      NULL when there is no word in the line
 */
extern char *get_word(char **line);

/*
 * line_header(line, header)
 *
 * check whether the specified line starts with a header ``header:''
 * the header is removed from the line
 *
 * return value:
 *      FN_SUCCESS ... yes
 *      FN_FAILURE ... no
 *
 * side effects:
 *      none
 */
int line_header(char **line, char *header);

/** Compare two strings and report if they match.
 * This function is a wrapper for strcmp() dealing better with NULL pointers.
 *
 * If any of the strings is NULL, they are considered not matching (even if
 * both of them are NULL).
 *
 * @param s1
 *      the first string.
 *
 * @param s2
 *      the second string.
 *
 * @return
 *      nonzero iff string match.
 */
int strmatch(const char *s1, const char *s2);

#endif

