/* $Id: proto.c,v 1.13 2006/08/15 14:34:20 jirka Exp $ */

/*
 * netrw tools
 * Copyright (C) 2002 Jiri Denemark
 *
 * This file is part of netrw.
 *
 * netrw is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#if HAVE_CONFIG_H
#include "config.h"
#endif

#if STDC_HEADERS
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#endif

#if !HAVE_SNPRINTF
/* use snprintf implementation bundled with netrw */
#include "snprintf.h"
#endif

#define NEED_PARAMS_ARRAY 1
#include "common.h"
#include "net.h"
#include "checksum.h"
#include "checksum_proto.h"
#include "keepalive.h"
#include "proto.h"
#include "proto_types.h"


#define STATE_NULL          (   0)
#define STATE_R_INIT        (   1)
#define STATE_R_PARAMS      (   2)
#define STATE_R_CHECKSUM    (   3)
#define STATE_R_INIT_SEND   (   4)
#define STATE_R_ACCEPT      (   5)
#define STATE_W_INIT        (  -1)
#define STATE_W_PARAMS      (  -2)
#define STATE_W_PAR_OPTIONS (  -3)
#define STATE_W_ACCEPT      (  -4)
#define STATE_INITIALIZED   ( 100)


#define STRLEN 1024
static char str[STRLEN];
static int state = STATE_NULL;

/* static functions */
static void get_params(struct proto_params *params, char *line);
static void param_options(int param,
                          char *options,
                          struct proto_params *req_params,
                          struct proto_params *params);
static int accept_option(int param,
                         char *options,
                         struct proto_params *req_params,
                         struct proto_params *params);


void params_initialize(struct proto_params *params)
{
#if USE_CHECKSUM_BY_DEFAULT
    params->checksum = 0;
#else
    params->checksum = CHECKSUM_NONE;
#endif
    params->keepalive = KEEPALIVE_ON;
}

void params_clear(struct proto_params *params)
{
    if (params->checksum != CHECKSUM_NONE) {
        print(VERBOSE, "checksum disabled\n");
        params->checksum = CHECKSUM_NONE;
    }

    params->keepalive = KEEPALIVE_OFF;
}

int proto_initialize(enum party who)
{
    if (state != STATE_NULL
        || (who != PARTY_NETWRITE && who != PARTY_NETREAD)) {
        print(ERROR, "Invalid protocol state in proto_initialize()\n");
        return FN_FAILURE;
    }

    if (who == PARTY_NETREAD)
        state = STATE_R_INIT;
    else if (who == PARTY_NETWRITE)
        state = STATE_W_INIT;

    return FN_SUCCESS;
}

void proto_finalize(void)
{
    state = STATE_NULL;
}

int proto_complete_init(struct proto_params *params)
{
    if (params == NULL)
        return FN_FAILURE;

    if (params->checksum != CHECKSUM_NONE) {
        print(VERBOSE, "trying to use %s checksum\n",
              CHECKSUMS[params->checksum]);
    }

    while (state < STATE_INITIALIZED) {
        if (proto_init_step(params) == FN_FAILURE)
            return FN_FAILURE;
    }

    return FN_SUCCESS;
}

int proto_init_step(struct proto_params *params)
{
    int i;
    int len = 0;
    static struct proto_params req_params = {
        PAR_NOT_REQ,
        PAR_NOT_REQ
    };
    char *line;

    switch (state) {
    case STATE_R_INIT:
        /* protocol header */
        if (control_write(SM_LATER, PROTO_INIT) == FN_FAILURE)
            return FN_FAILURE;

        state = STATE_R_PARAMS;
        break;

    case STATE_R_PARAMS:
        /* requested parameters */
        len = snprintf(str, STRLEN, PROTO_REQ_PARAMS ":");

        if (params->checksum != CHECKSUM_NONE) {
            req_params.checksum = PAR_REQUESTED;
            len += snprintf(str + len, STRLEN - len, " " PAR_STR_CHECKSUM);
        }

        if (params->keepalive == KEEPALIVE_ON) {
            req_params.keepalive = PAR_REQUESTED;
            len += snprintf(str + len, STRLEN - len, " " PAR_STR_KEEPALIVE);
        }

        str[len] = '\0';

        if (control_write(SM_LATER, str) == FN_FAILURE)
            return FN_FAILURE;

        state = STATE_R_CHECKSUM;
        break;

    case STATE_R_CHECKSUM:
        /* available checksum algorithms */
        len = snprintf(str, STRLEN, PAR_STR_CHECKSUM ":");
        /* preferred algorithm first */
        if (params->checksum != CHECKSUM_NONE) {
            len += snprintf(str + len, STRLEN - len,
                            " %s", CHECKSUMS[params->checksum]);
        }
        /* all other algorithms */
        for (i = 0; i < CHECKSUMS_CNT; i++) {
            if (i == params->checksum)
                continue;
            len += snprintf(str + len, STRLEN - len, " %s", CHECKSUMS[i]);
        }

        if (control_write(SM_LATER, str) == FN_FAILURE)
            return FN_FAILURE;

        state = STATE_R_INIT_SEND;
        break;

    case STATE_R_INIT_SEND:
        /* empty line indicates end of request */
        if (control_write(SM_IMMEDIATELY, "") == FN_FAILURE)
            return FN_FAILURE;

        state = STATE_R_ACCEPT;
        break;

    case STATE_W_INIT:
        /* get and compare protocol header */
        if (!strmatch((line = control_read()), PROTO_INIT)) {
            if (line != NULL)
                print(ERROR, "Invalid protocol header: ``%s''\n", line);
            return FN_FAILURE;
        }

        state = STATE_W_PARAMS;
        break;

    case STATE_W_PARAMS:
        /* get params requested by remote party */
        if ((line = control_read()) == NULL
            || line_header(&line, PROTO_REQ_PARAMS) == FN_FAILURE) {
            print(ERROR, "Missing connection parameters\n");
            return -1;
        }
        get_params(&req_params, line);

        state = STATE_W_PAR_OPTIONS;
        break;

    case STATE_W_PAR_OPTIONS:
        /* parse parameters options */
        while ((line = control_read()) != NULL
               && line[0] != '\0') {
            for (i = 0; i < PARAMS_CNT; i++) {
                if (line_header(&line, PARAMS[i]) == FN_SUCCESS)
                    break;
            }
            if (i == PARAMS_CNT) {
                print(WARNING, "Unknown connection parameter: ``%s''; "
                      "ignored\n", line);
                continue;
            }

            param_options(i, line, &req_params, params);
        }
        if (line == NULL)
            return FN_FAILURE;

        checksum_proto_check(&req_params, params);
        keepalive_proto_check(&req_params, params);

        state = STATE_W_ACCEPT;
        break;

    case STATE_W_ACCEPT:
        /* protocol header */
        if (control_write(SM_LATER, PROTO_INIT) == FN_FAILURE)
            return FN_FAILURE;

        /* accepted connection parametres */
        if (params->checksum != CHECKSUM_NONE) {
            if (control_writef(SM_LATER, "%s: %s", PAR_STR_CHECKSUM,
                               CHECKSUMS[params->checksum]) == FN_FAILURE)
                return FN_FAILURE;
        }
        if (params->keepalive == KEEPALIVE_ON) {
            if (control_writef(SM_LATER, "%s: on",
                               PAR_STR_KEEPALIVE) == FN_FAILURE)
                return FN_FAILURE;
        }

        /* empty line indicates end of request */
        if (control_write(SM_IMMEDIATELY, "") == FN_FAILURE)
            return FN_FAILURE;

        state = STATE_INITIALIZED;
        break;

    case STATE_R_ACCEPT:
        /* get and compare protocol header */
        if (!strmatch((line = control_read()), PROTO_INIT)) {
            if (line != NULL)
                print(ERROR, "Invalid protocol header: ``%s''\n", line);
            return FN_FAILURE;
        }

        /* clear params */
        params->checksum = CHECKSUM_NONE;
        req_params.checksum = PAR_NOT_REQ;
        params->keepalive = KEEPALIVE_OFF;
        req_params.keepalive = PAR_NOT_REQ;

        /* parse accepted options */
        while ((line = control_read()) != NULL
               && line[0] != '\0') {
            for (i = 0; i < PARAMS_CNT; i++) {
                if (line_header(&line, PARAMS[i]) == FN_SUCCESS)
                    break;
            }
            if (i == PARAMS_CNT) {
                print(ERROR, "Unknown connection parameter: ``%s''\n", line);
                return FN_FAILURE;
            }

            if (accept_option(i, line, &req_params, params) == FN_FAILURE)
                return FN_FAILURE;
        }
        if (line == NULL)
            return FN_FAILURE;

        keepalive_proto_check(&req_params, params);

        state = STATE_INITIALIZED;
        break;

    default:
        print(ERROR, "Invalid protocol state in proto_init_step(): %d\n", state);
        return FN_FAILURE;
    }

    return FN_SUCCESS;
}


static void get_params(struct proto_params *params, char *line)
{
    char *word;
    int i;

    while ((word = get_word(&line)) != NULL) {
        for (i = 0; i < PARAMS_CNT; i++) {
            if (strmatch(word, PARAMS[i]))
                break;
        }

        switch (i) {
        case PARAM_CHECKSUM:
            params->checksum = PAR_REQUESTED;
            break;

        case PARAM_KEEPALIVE:
            params->keepalive = PAR_REQUESTED;
            break;

        default:
            print(WARNING, "Unknown connection parameter requested: ``%s''; "
                  "ignored\n", word);
        }
    }
}

static void param_options(int param,
                          char *options,
                          struct proto_params *req_params,
                          struct proto_params *params)
{
    switch (param) {
    case PARAM_CHECKSUM:
        checksum_proto_param_options(options, req_params, params);
        break;
    }
}

static int accept_option(int param,
                         char *options,
                         struct proto_params *req_params,
                         struct proto_params *params)
{
    char *option;

    option = get_word(&options);

    switch (param) {
    case PARAM_CHECKSUM:
        return checksum_proto_accept_option(option, req_params, params);

    case PARAM_KEEPALIVE:
        req_params->keepalive = PAR_REQUESTED;
        return FN_SUCCESS;
    }

    return FN_FAILURE;
}

