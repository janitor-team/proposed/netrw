/* $Id: common.c,v 1.4 2006/08/15 14:34:20 jirka Exp $ */

/*
 * netrw tools
 * Copyright (C) 2002 Jiri Denemark
 *
 * This file is part of netrw.
 *
 * netrw is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#if HAVE_CONFIG_H
#include "config.h"
#endif

#if STDC_HEADERS
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#endif

#include <stdarg.h>

#include "common.h"


/*****************************************************************************
 * printing with verbosity
 */

static int verbosity = NORMAL;

void set_verbosity(int v)
{
    switch (v) {
    case QUIET:
        verbosity = QUIET;
        break;
    case VERBOSE:
        verbosity++;
        break;
    }
}

int can_print(int verbose_level)
{
    return (verbosity >= verbose_level);
}

void print(int verbose_level, char *msg, ...)
{
    va_list args;

    if (!msg || !can_print(verbose_level))
        return;

    switch (verbose_level) {
    case ERROR:
        fprintf(stderr, "ERROR: ");
        break;
    case WARNING:
        fprintf(stderr, "WARNING: ");
        break;
    /*
    default:
        print nothing
    */
    }

    va_start(args, msg);
    vfprintf(stderr, msg, args);
    va_end(args);

    fflush(stderr);
}


/*****************************************************************************
 * string parsing
 */

char *get_word(char **line)
{
    char *word;

    while (**line == ' ')
        (*line)++;
    word = *line;

    while (**line != ' ' && **line != '\0')
        (*line)++;
    if (**line == ' ') {
        **line = '\0';
        (*line)++;
    }

    if (word[0] == '\0')
        return NULL;
    else
        return word;
}

int line_header(char **line, char *header)
{
    int len = strlen(header);

    if (strncmp(*line, header, len) != 0
        || strlen(*line) <= len
        || (*line)[len] != ':')
        return FN_FAILURE;
    else {
        *line += len + 1;
        return FN_SUCCESS;
    }
}


int strmatch(const char *s1, const char *s2)
{
    if (s1 == NULL || s2 == NULL)
        return 0;
    else
        return (strcmp(s1, s2) == 0);
}

