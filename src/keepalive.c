/* $Id: keepalive.c,v 1.3 2006/08/15 14:34:20 jirka Exp $ */
/*
 * netrw tools.
 * Copyright (C) 2006 Jiri Denemark
 *
 * This file is part of netrw.
 *
 * netrw is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/** @file
 * netrw tools.
 * @author Jiri Denemark
 * @date 2006
 * @version $Revision: 1.3 $ $Date: 2006/08/15 14:34:20 $
 */

#if HAVE_CONFIG_H
# include "config.h"
#endif

#if STDC_HEADERS
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
#endif
#if HAVE_UNISTD_H
# include <unistd.h>
#endif
#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif

#include "common.h"
#include "net.h"
#include "proto_types.h"
#include "keepalive.h"


#define KEEPALIVE_INTERVAL  60
#define KEEPALIVE_END_MARK  "finished"

static time_t last = 0;
static int finished = 0;


void keepalive_proto_check(struct proto_params *req_params,
                           struct proto_params *params)
{
    if (req_params->keepalive == PAR_REQUESTED) {
        params->keepalive = KEEPALIVE_ON;
        print(VERBOSE, "will send keep-alive packets through control "
              "connection\n");
    }
    else {
        params->keepalive = KEEPALIVE_OFF;
        print(WARNING, "Remote party has no support for keep-alive packets\n");
        print(WARNING, "Expect problems with control connection and stupid "
              "firewalls\n");
    }
}


void keepalive_write(void)
{
    time_t now = time(NULL);

    if (last + KEEPALIVE_INTERVAL <= now) {
        last = now;
        control_writef(SM_IMMEDIATELY, "%s: %ld", PAR_STR_KEEPALIVE, last);
    }
}


void keepalive_write_finish(void)
{
    control_writef(SM_IMMEDIATELY, "%s: %s\n",
                   PAR_STR_KEEPALIVE, KEEPALIVE_END_MARK);
}


void keepalive_read(void)
{
    char *line;

    if (finished || control_check_read() == FN_FAILURE)
        return;

    if ((line = control_read()) == NULL) {
        finished = 1;
        return;
    }

    if (line_header(&line, PAR_STR_KEEPALIVE) == FN_FAILURE) {
        print(WARNING, "Keep-alive packet expected: ``%s''\n", line);
        return;
    }

    if (strmatch(get_word(&line), KEEPALIVE_END_MARK))
        finished = 1;
}


void keepalive_read_finish(void)
{
    char *line;

    while (!finished)
        keepalive_read();

    /* eat up empty line */
    if ((line = control_read()) != NULL && *line != '\0')
        print(WARNING, "Empty line expected: ``%s''\n", line);
}

