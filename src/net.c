/* $Id: net.c,v 1.14 2006/03/02 23:11:46 jirka Exp $ */

/*
 * netrw tools
 * Copyright (C) 2002 Jiri Denemark
 *
 * This file is part of netrw.
 *
 * netrw is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#if HAVE_CONFIG_H
#include "config.h"
#endif

#if STDC_HEADERS
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#endif

#include <errno.h>
#if HAVE_NETDB_H
#include <netdb.h>
#endif
#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif
#if HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#if HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif
#if HAVE_UNISTD_H
#include <unistd.h>
#endif
#if HAVE_SYS_SELECT_H
# include <sys/select.h>
#endif
#if HAVE_STDARG_H
# include <stdarg.h>
#endif

#if !HAVE_VSNPRINTF
/* use vsnprintf implementation bundled with netrw */
#include "vsnprintf.h"
#endif

#include "common.h"
#include "net.h"


/* SHUT_RD and SHUT_WR are defined since glibc-2.1.91 */
#ifndef SHUT_RD
#define SHUT_RD 0
#endif
#ifndef SHUT_WR
#define SHUT_WR 1
#endif
#ifndef SHUT_RDWR
#define SHUT_RDWR 2
#endif


/* connection details */
static enum connection_mode conn_mode;
static enum transport_protocol trans_protocol;
static char *tp_str = NULL;
static unsigned short port = 0;
static struct in_addr ip;

/* sockets */
static int s_listen = -1;
static int s_control = -1;
static int s_data = -1;

/* control conection IO */
static char *r_buf;
static char *r_tmp;
static int r_bufpos = 0;
static int r_buflen = 0;
static char *w_buf;
static char *w_tmp;
static int w_buflen = 0;


/*****************************************************************************
 * host, transport protocol, and service
 */

int set_host(char *host)
{
    struct hostent *he;

    if (!(he = gethostbyname(host))) {
        print(ERROR, "Cannot resolve host name (%s): %s\n",
              host, hstrerror(h_errno));
        return FN_FAILURE;
    }

    ip = **(struct in_addr **) he->h_addr_list;

    print(VVERBOSE, "host IP address: %s\n", inet_ntoa(ip));

    return FN_SUCCESS;
}

int set_service(char *service, enum transport_protocol tp)
{
    unsigned short p;
    struct servent *se;

    trans_protocol = tp;
    switch (tp) {
    case TP_UDP:
        tp_str = "udp";
        break;
    case TP_TCP:
        tp_str = "tcp";
        break;
    }

    if ((se = getservbyname(service, tp_str)))
        p = se->s_port;
    else {
        /* service not found, but it may also be a port number itself */
        char *endptr = NULL;
        long i;

        i = strtol(service, &endptr, 0);
        if (*service == '\0' || *endptr != '\0'
            || i <= 0 || i > 65535) {
            print(ERROR, "No such service: %s/%s\n", service, tp_str);
            return FN_FAILURE;
        }
        else
            p = htons(i);
    }

    port = p;

    print(VVERBOSE, "port number: %d/%s\n", ntohs(port), tp_str);

    return FN_SUCCESS;
}

int in_addr(struct sockaddr_in *sin, enum ip_mode mode)
{
    if (sin == NULL || port == 0)
        return FN_FAILURE;

    memset(sin, '\0', sizeof(struct sockaddr_in));

    sin->sin_family = AF_INET;
    sin->sin_port = port;

    switch (mode) {
    case IM_ANY:
        sin->sin_addr.s_addr = INADDR_ANY;
        break;

    case IM_HOST:
        sin->sin_addr = ip;
        break;
    }

    return FN_SUCCESS;
}


/*****************************************************************************
 * connection, and socket
 */

int create_data_socket(enum connection_mode mode)
{
    int s;
    unsigned int size;
    struct sockaddr_in sin;
    int reuse_addr = 1;

    conn_mode = mode;

    print(VVERBOSE, "creating data socket\n");

    switch (trans_protocol) {
    case TP_UDP:
        if ((s = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
            print(ERROR, "Cannot create data socket: %s\n", strerror(errno));
            return FN_FAILURE;
        }

        if (mode == CM_READER) {
            if (in_addr(&sin, IM_ANY) == FN_FAILURE)
                return FN_FAILURE;

            if (bind(s, (struct sockaddr *) &sin, sizeof(sin))) {
                print(ERROR, "Cannot bind socket to %s:%d: %s\n",
                      inet_ntoa(sin.sin_addr), ntohs(sin.sin_port),
                      strerror(errno));
                return FN_FAILURE;
            }
        }
        break;

    case TP_TCP:
        switch (mode) {
        case CM_WRITER:
        case CM_READER_FW:
            if ((s = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
                print(ERROR, "Cannot create data socket: %s\n",
                      strerror(errno));
                return FN_FAILURE;
            }

            if (in_addr(&sin, IM_HOST) == FN_FAILURE)
                return FN_FAILURE;

            if (connect(s, (struct sockaddr *) &sin, sizeof(sin))) {
                print(ERROR, "Cannot connect to %s:%d: %s\n",
                      inet_ntoa(sin.sin_addr), ntohs(sin.sin_port),
                      strerror(errno));
                return FN_FAILURE;
            }

            print(VERBOSE, "connected to %s:%d\n",
                  inet_ntoa(sin.sin_addr), ntohs(sin.sin_port));
            break;

        case CM_READER:
        case CM_WRITER_FW:
            if ((s_listen = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
                print(ERROR, "Cannot create listening socket: %s\n",
                      strerror(errno));
                return FN_FAILURE;
            }

            if (setsockopt(s_listen, SOL_SOCKET, SO_REUSEADDR,
                           (void *) &reuse_addr, sizeof(reuse_addr))) {
                print(WARNING, "Cannot set SO_REUSEADDR option: %s\n",
                      strerror(errno));
            }

            if (in_addr(&sin, IM_ANY) == FN_FAILURE)
                return FN_FAILURE;

            if (bind(s_listen, (struct sockaddr *) &sin, sizeof(sin))) {
                print(ERROR, "Cannot bind socket to %s:%d: %s\n",
                      inet_ntoa(sin.sin_addr), ntohs(sin.sin_port),
                      strerror(errno));
                return FN_FAILURE;
            }

            if (listen(s_listen, 2)) {
                print(ERROR, "Cannot listen on %d: %s\n",
                      ntohs(sin.sin_port), strerror(errno));
                return FN_FAILURE;
            }

            print(VVERBOSE, "listening socket created\n");
            print(VERBOSE, "listening on %d\n", ntohs(port));

            size = sizeof(sin);
            if ((s = accept(s_listen, (struct sockaddr *) &sin,
                            &size)) == -1) {
                print(ERROR, "Cannot accept data connection: %s\n",
                      strerror(errno));
                return FN_FAILURE;
            }

            print(VERBOSE, "accepted data connection from %s:%d\n",
                  inet_ntoa(sin.sin_addr), ntohs(sin.sin_port));
            break;

        default:
            return FN_FAILURE;
        }
        break;

    default:
        return FN_FAILURE;
    }

    s_data = s;

    print(VVERBOSE, "%s data socket created\n", tp_str);

    return FN_SUCCESS;
}

int close_data_socket(void)
{
    int ret;
    
    if (trans_protocol == TP_TCP)
        shutdown(s_data, SHUT_RDWR);

    ret = close(s_data);
    s_data = -1;

    if (ret)
        return FN_SUCCESS;
    else
        return FN_FAILURE;
}

int have_data_socket(void)
{
    return (s_data > -1);
}

int create_control_socket(void)
{
    int n;
    int s;
    unsigned int size;
    struct sockaddr_in sin;
    fd_set rfds;
    fd_set efds;
    struct timeval timeout;
    struct timeval *t_out;

    print(VVERBOSE, "creating control socket\n");

    switch (trans_protocol) {
    case TP_UDP:
        return FN_FAILURE;

    case TP_TCP:
        switch (conn_mode) {
        case CM_WRITER:
        case CM_READER_FW:
            if ((s = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
                print(ERROR, "Cannot create control socket: %s\n",
                      strerror(errno));
                return FN_FAILURE;
            }

            if (in_addr(&sin, IM_HOST) == FN_FAILURE)
                return FN_FAILURE;

            if (connect(s, (struct sockaddr *) &sin, sizeof(sin))) {
                print(ERROR, "Cannot connect to %s:%d: %s\n",
                      inet_ntoa(sin.sin_addr), ntohs(sin.sin_port),
                      strerror(errno));
                return FN_FAILURE;
            }

            print(VERBOSE, "connected to %s:%d\n",
                  inet_ntoa(sin.sin_addr), ntohs(sin.sin_port));
            break;

        case CM_READER:
        case CM_WRITER_FW:
            FD_ZERO(&rfds);
            FD_ZERO(&efds);
            FD_SET(s_listen, &rfds);
            FD_SET(s_listen, &efds);
            if (conn_mode == CM_READER) {
                FD_SET(s_data, &rfds);
                FD_SET(s_data, &efds);
                n = max(s_listen, s_data) + 1;
                t_out = NULL;
            }
            else {
                n = s_listen + 1;
                timeout.tv_sec = 10;
                timeout.tv_usec = 0;
                t_out = &timeout;
            }

            if (select(n, &rfds, NULL, &efds, t_out) >= 0) {
                if (FD_ISSET(s_listen, &rfds) && !FD_ISSET(s_data, &rfds)) {
                    size = sizeof(sin);
                    if ((s = accept(s_listen, (struct sockaddr *) &sin,
                                    &size)) == -1) {
                        print(ERROR, "Cannot accept control connection: %s\n",
                              strerror(errno));
                        close(s_listen);
                        s_listen = -1;
                        return FN_FAILURE;
                    }

                    print(VERBOSE, "accepted control connection from %s:%d\n",
                          inet_ntoa(sin.sin_addr), ntohs(sin.sin_port));
                }
                else {
                    print(ERROR, "Cannot create control socket: remote "
                          "party does not support control connection\n");
                    close(s_listen);
                    s_listen = -1;
                    return FN_FAILURE;
                }
            }
            else {
                print(ERROR, "Cannot create control socket: select() "
                      "failed: %s\n", strerror(errno));
                close(s_listen);
                s_listen = -1;
                return FN_FAILURE;
            }

            close(s_listen);
            s_listen = -1;
            break;

        default:
            return FN_FAILURE;
        }
        break;

    default:
        return FN_FAILURE;
    }

    /* initialize IO buffers for control connection */
    if ((r_buf = (char *) malloc(BUFFSIZE)) == NULL
        || (w_buf = (char *) malloc(BUFFSIZE)) == NULL
        || (r_tmp = (char *) malloc(BUFFSIZE)) == NULL
        || (w_tmp = (char *) malloc(BUFFSIZE)) == NULL) {
        print(ERROR, "Cannot allocate control connection IO buffers: %s\n",
              strerror(errno));
        return FN_FAILURE;
    }
    r_buf[0] = '\0';
    w_buf[0] = '\0';
    r_tmp[0] = '\0';
    w_tmp[0] = '\0';
                
    s_control = s;

    print(VVERBOSE, "control socket created\n");

    return FN_SUCCESS;
}

int close_control_socket(void)
{
    int ret;

    /* free IO buffers for control conection */
    free(r_buf);
    r_buf = NULL;
    free(w_buf);
    w_buf = NULL;
    free(r_tmp);
    r_tmp = NULL;

    if (trans_protocol == TP_TCP)
        shutdown(s_control, SHUT_RDWR);
    ret = close(s_control);
    s_control = -1;

    if (ret)
        return FN_SUCCESS;
    else
        return FN_FAILURE;
}

int have_control_socket(void)
{
    return (s_control > -1);
}


/*****************************************************************************
 * data connection IO
 */

int data_wait(void)
{
    fd_set e_fds;
    fd_set r_fds;

    FD_ZERO(&e_fds);
    FD_SET(s_data, &e_fds);

    FD_ZERO(&r_fds);
    FD_SET(s_data, &r_fds);

    if (select(s_data + 1, &r_fds, NULL, &e_fds, NULL) > 0) {
        if (FD_ISSET(s_data, &r_fds))
            return FN_SUCCESS;
    }
    else
        print(ERROR, "select() failed: %s\n", strerror(errno));

    return FN_FAILURE;
}

ssize_t data_read(void *buf, size_t count,
                  struct sockaddr_in *from, socklen_t *fromlen)
{
    ssize_t ret = 0;

    switch (trans_protocol) {
    case TP_UDP:
        ret = recvfrom(s_data, buf, count, 0,
                       (struct sockaddr *) from, fromlen);
        break;

    case TP_TCP:
        ret = read(s_data, buf, count);
        break;
    }

    if (ret < 0)
        print(ERROR, "Cannot read data: %s\n", strerror(errno));

    return ret;
}

ssize_t data_write(const void *buf, size_t count)
{
    ssize_t ret = 0;
    struct sockaddr_in to;

    switch (trans_protocol) {
    case TP_UDP:
        in_addr(&to, IM_HOST);
        ret = sendto(s_data, buf, count, 0,
                     (struct sockaddr *) &to, sizeof(to));
        break;

    case TP_TCP:
        ret = write(s_data, buf, count);
        break;
    }

    if (ret < 0)
        print(ERROR, "Cannot write data: %s\n", strerror(errno));

    return ret;
}


/*****************************************************************************
 * control connection IO
 */

int control_check_read(void)
{
    struct timeval timeout = { 0, 0 };
    fd_set rfds;
    fd_set efds;

    if (!have_control_socket())
        return FN_FAILURE;

    if (r_bufpos < r_buflen)
        return FN_SUCCESS;

    FD_ZERO(&rfds);
    FD_ZERO(&efds);
    FD_SET(s_control, &rfds);
    FD_SET(s_control, &efds);

    if (select(s_control + 1, &rfds, NULL, &efds, &timeout) > 0
        && (FD_ISSET(s_control, &rfds) || FD_ISSET(s_control, &efds)))
        return FN_SUCCESS;
    else
        return FN_FAILURE;
}

char *control_read(void)
{
    static int closed = 0;
    int ignoring = 0;
    int linepos = 0;

    if (closed)
        return NULL;

    r_buf[r_buflen] = '\0';

    while (r_buf[r_bufpos] != '\n' || ignoring) {
        if (ignoring && r_buf[r_bufpos] == '\n') {
            /* newline found, end of ignored data */
            ignoring = 0;
            r_bufpos++;
            continue;
        }

        if (r_bufpos == r_buflen) {
            /* end of buffer reached */
            if ((r_buflen = read(s_control, r_buf, BUFFSIZE - 1)) < 0) {
                print(ERROR, "Cannot read data from control connection: %s\n",
                      strerror(errno));
                r_buflen = r_bufpos;
                return NULL;
            }
            else if (r_buflen == 0) {
                print(ERROR, "Control connection closed by foreign host\n");
                r_bufpos = 0;
                closed = 1;
                return NULL;
            }

            r_buf[r_buflen] = '\0';
            r_bufpos = 0;
            continue;
        }

        if (linepos == BUFFSIZE - 1) {
            /* line buffer is full and no newline (LF) character was found
               possible attack, ignore all of the data till the first LF */
            print(WARNING, "Line too long, will be ignored\n");
            linepos = 0;
            ignoring = 1;
        }

        if (!ignoring) {
            r_tmp[linepos] = r_buf[r_bufpos];
            linepos++;
        }
        r_bufpos++;
    }

    /* remove possible trailing CR character */
    if (linepos > 0 && r_tmp[linepos - 1] == '\r')
        linepos--;

    r_tmp[linepos] = '\0';
    r_bufpos++;

    return r_tmp;
}

int control_write(enum send_mode mode, char *line)
{
    int len;
    int wr;
    char *p;

    if ((len = strlen(line)) + 1 > BUFFSIZE) {
        print(ERROR, "Line too long, would be ignored on receive\n");
        return FN_FAILURE;
    }

    /* check whether there is enough space for this line in the buffer
       if not send the buffer first */
    if (w_buflen + len + 1 >= BUFFSIZE) {
         for (p = w_buf; (wr = write(s_control, p, w_buflen)) > 0; p += wr) {
             if ((w_buflen -= wr) == 0)
                 break;
         }
         w_buflen = 0;

         if (wr < 0) {
             print(ERROR, "Cannot write data to control connection: %s\n",
                   strerror(errno));
             return FN_FAILURE;
         }
    }

    /* append the line to the end of the buffer */
    memcpy(w_buf + w_buflen, line, len);
    w_buflen += len;
    w_buf[w_buflen++] = '\n';

    if (mode == SM_IMMEDIATELY) {
        for (p = w_buf; (wr = write(s_control, p, w_buflen)) > 0; p += wr) {
            if ((w_buflen -= wr) == 0)
                break;
        }
        w_buflen = 0;
 
        if (wr < 0) {
            print(ERROR, "Cannot write data to control connection: %s\n",
                  strerror(errno));
            return FN_FAILURE;
        }
    }

    return FN_SUCCESS;
}

int control_writef(enum send_mode mode, char *fmt, ...)
{
    va_list args;
    int len;

    va_start(args, fmt);
    len = vsnprintf(w_tmp, BUFFSIZE, fmt, args);
    va_end(args);

    if (len + 1 > BUFFSIZE) {
        print(ERROR, "Line too long, would be ignored\n");
        return FN_FAILURE;
    }

    return control_write(mode, w_tmp);
}

