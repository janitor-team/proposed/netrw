/* $Id: progress.h,v 1.6 2004/07/17 18:24:56 jirka Exp $ */

/*
 * netrw tools
 * Copyright (C) 2002 Jiri Denemark
 *
 * This file is part of netrw.
 *
 * netrw is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef PROGRESS_H
#define PROGRESS_H

#define DEFAULT_HASH_AFTER  10737418  /* in B ~ 10.24 MiB */

/* option processing */
#define OPTION_b
#define SPEED_OPT "b"
#define SPEED_USAGE \
    "  -b               print speed in b/s instead of B/s\n"
#define SPEED_CASE  \
    case 'b':           /* print speed in b/s */            \
        progress_speed_bps();                               \
        break;

#define OPTION_H
#define OPTION_h
#define HASH_OPT "H:h:"
#define HASH_USAGE \
    "  -h <n>           print `#' after each n KiB transferred (def. 10485.76).\n" \
    "  -H <n>           print `#' after each n MiB transferred (def. 10.24).\n"
#define HASH_CASE \
    case 'H':           /* print hash after n MB */         \
        progress_hash_after(atoi(optarg) * 1024 * 1024);    \
        break;                                              \
    case 'h':           /* print hash after n KB */         \
        progress_hash_after(atoi(optarg) * 1024);           \
        break;


/*
 * progress_speed_bps()
 *
 * set unit for speed printing to bits per second
 *
 * return value:
 *      none
 *
 * side effects:
 *      sets static variables bps, unit
 */
extern void progress_speed_bps(void);

/*
 * progress_hash_after(bytes)
 *
 * set the amount of data after that a hash has to be printed
 *
 * return value:
 *      none
 *
 * side effects:
 *      sets static variable after
 */
extern void progress_hash_after(unsigned long bytes);

/*
 * progress_data(transferred_bytes)
 *
 * inform the user that the specified amount of data was transferred
 * print hashes if it is needed
 *
 * return value:
 *      none
 *
 * side effects:
 *      sets static variables bytes, hashes, start
 */
extern void progress_data(unsigned long transferred_bytes);

/*
 * progress_summary()
 *
 * print summary of the data transmission (speed, time, ...)
 *
 * return value:
 *      none
 *
 * side effects:
 *      none
 */
extern void progress_summary(void);

#endif

