/* $Id: netread.c,v 1.13 2006/08/15 14:34:20 jirka Exp $ */

/*
 * netrw tools
 * Copyright (C) 2002 Jiri Denemark
 *
 * This file is part of netrw.
 *
 * netrw is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#if HAVE_CONFIG_H
#include "config.h"
#endif

#if STDC_HEADERS
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#endif

#if HAVE_UNISTD_H
#include <unistd.h>
#endif

#if HAVE_GETOPT_H
#include <getopt.h>
#elif !HAVE_GETOPT
/* use getopt implementation bundled with netrw */
#include "getopt.h"
#endif

#if HAVE_SYS_TYPES_H
# include <sys/types.h>
#endif
#if HAVE_SYS_STAT_H
# include <sys/stat.h>
#endif
#if HAVE_FCNTL_H
# include <fcntl.h>
#endif

#include <errno.h>


#include "common.h"
#include "progress.h"
#include "net.h"
#include "proto.h"
#include "proto_types.h"
#include "checksum.h"
#include "keepalive.h"
#include "version.h"

static void usage(char *prog_name)
{
    fprintf(stderr,
        "usage\n"
        "  %s [udp] <options> <port>\n"
        "\n"
        "Default TCP protocol can be changed to UDP by ``udp'' argument.\n"
        "UDP options\n"
        "  currently none\n"
        "TCP options\n"
        "  -f <host>        firewall mode, connection is initiated by netread.\n"
        CHECKSUM_USAGE
        "general options\n"
        "  -o <file>        write data to file instead of stdout.\n"
        "  -s               in case -o option is specified, open the file for\n"
        "                   synchronous I/O. Otherwise, this option has no effect.\n"
        SPEED_USAGE
        HASH_USAGE
        VERBOSITY_USAGE
        VERSION_USAGE
        RETURN_VALUES,
        prog_name);
    exit(1);
}

int main(int argc, char **argv)
{
    int udp = 0;
    int opt;
    int firewall = 0;
    char *host = NULL;
    char *service = NULL;
    struct sockaddr_in r_addr;
    socklen_t addrsize = sizeof(r_addr);
    char buf[BUFFSIZE];
    ssize_t count;
    struct proto_params params;
    int fd = STDOUT_FILENO;
    const char *file = NULL;
    int sync = 0;
  
    if (argc < 2)
        usage(argv[0]);

    if (strmatch(argv[1], "udp")) {
        udp = 1;
        argv[1] = argv[0];
    }

    params_initialize(&params);

    while ((opt = getopt(argc - udp, argv + udp, "f:o:s"
                         VERSION_OPT SPEED_OPT HASH_OPT VERBOSITY_OPT
                         CHECKSUM_OPT)) != -1) {
        switch (opt) {
        case 'f':           /* firewall mode */
            if (udp)
                usage(argv[0]);
            firewall = 1;
            host = optarg;
            break;

        case 'o':           /* output file */
            file = optarg;
            break;

        case 's':           /* synchronous output */
            sync = O_SYNC;
            break;

        VERSION_CASE        /* version */
        SPEED_CASE          /* units for speed printing */
        HASH_CASE           /* hash printing options */
        VERBOSITY_CASE      /* verbosity options */
        CHECKSUM_CASE       /* checksum options */

        case '?':           /* bad option */
            usage(argv[0]);
        }
    }

    if ((optind + udp) >= argc
        || (service = argv[optind + udp]) == NULL)
        usage(argv[0]);

    if (file != NULL &&
        (fd = open(file, O_WRONLY | O_CREAT | O_TRUNC | sync, 0666)) == -1) {
        print(ERROR, "Cannot open ouput file: %s: %s\n",
              strerror(errno), file);
        return 1;
    }

    if (set_service(service, (udp ? TP_UDP : TP_TCP)) == FN_FAILURE)
        return 1;

    /* create socket */
    if (udp) {
        if (create_data_socket(CM_READER) == FN_FAILURE)
            return 1;

        print(VERBOSE, "UDP protocol requested\n");
        params_clear(&params);
    }
    else if (firewall) {
        if (set_host(host) == FN_FAILURE
            || create_data_socket(CM_READER_FW) == FN_FAILURE)
            return 1;
    }
    else {
        if (create_data_socket(CM_READER) == FN_FAILURE)
            return 1;
    }

    if (!udp) {
        if (create_control_socket() == FN_FAILURE
            || proto_initialize(PARTY_NETREAD) == FN_FAILURE
            || proto_complete_init(&params) == FN_FAILURE) {
            if (have_control_socket()) {
                proto_finalize();
                print(ERROR, "Closing control connection\n");
                close_control_socket();
            }

            params_clear(&params);
        }
    }

    if (params.checksum != CHECKSUM_NONE)
        checksum_init(&params);

    while ((count = data_read(buf, BUFFSIZE, &r_addr, &addrsize)) > 0) {
        int wr;
        char *p;

        if (udp) {
            print(VERBOSE, "message from %s:%d\n",
                  inet_ntoa(r_addr.sin_addr), ntohs(r_addr.sin_port));
        }

        progress_data(count);

        if (params.checksum != CHECKSUM_NONE)
            checksum_process((void *) buf, count);

        if (params.keepalive == KEEPALIVE_ON)
            keepalive_read();

        for (p = buf; (wr = write(fd, p, count)) > 0; p += wr) {
            if (!(count -= wr))
                break;
        }
        if (wr < 0) {
            print(ERROR, "Cannot write data: %s\n", strerror(errno));
            return 1;
        }
    }

    if (file != NULL)
        close(fd);

    progress_summary();

    if (count < 0)
        return 1;

    close_data_socket();

    if (params.keepalive == KEEPALIVE_ON)
        keepalive_read_finish();

    if (params.checksum != CHECKSUM_NONE) {
        char *sum;

        print(NORMAL, "\n");
        if ((sum = checksum_finish()) == NULL)
            sum = "failed";
        print(VERBOSE, "transmission checksum: %s\n", sum);
        if ((sum = checksum_exchange()) == NULL)
            sum = "failed";
        print(VERBOSE, "remote party checksum: %s\n", sum);

        if (checksum_validate() == FN_SUCCESS) {
            print(NORMAL, "%s checksum validation successful\n",
                  CHECKSUMS[params.checksum]);
        }
        else {
            print(WARNING, "%s checksum validation failed\n",
                  CHECKSUMS[params.checksum]);
            return 2;
        }
    }

    if (have_control_socket()) {
        proto_finalize();
        close_control_socket();
    }

    return 0;
}

