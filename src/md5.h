/* $Id: md5.h,v 1.4 2003/04/22 15:28:38 xdenemar Exp $ */

#ifndef MD5_H
#define MD5_H

/* Slightly modified MD5 implementation from mhash library. */

#define MD5_SIZE 16

typedef UINT_32 word32;

struct MD5Context {
    word32 buf[4];
    word32 bits[2];
    unsigned char in[64];
};

extern void MD5Init(struct MD5Context *context);

extern void MD5Update(struct MD5Context *context, unsigned char const *buf,
                      unsigned len);

extern void MD5Final(struct MD5Context *context, unsigned char *digest);

extern void MD5String(const unsigned char *digest, char *str);

/*
 * This is needed to make RSAREF happy on some MS-DOS compilers.
 */
typedef struct MD5Context MD5_CTX;

#endif /* !MD5_H */

