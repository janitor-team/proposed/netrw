/* $Id: checksum.h,v 1.14 2006/08/15 14:34:20 jirka Exp $ */

/*
 * netrw tools
 * Copyright (C) 2002 Jiri Denemark
 *
 * This file is part of netrw.
 *
 * netrw is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef CHECKSUM_H
#define CHECKSUM_H

#include "proto_types.h"
#include "common.h"


/* protocol parameters */
#define PAR_STR_CHECKSUM  "checksum"

#define CHECKSUM_NONE   (-1)

#if USE_CHECKSUM_MHASH
#define CHECKSUMS_LIST  "sha256", "sha1", "md5", "rmd160", "tiger", "gost"
#define CHECKSUMS_STR   "sha256 sha1 md5 rmd160 tiger gost"
#define CHECKSUM_SHA256 ( 0)
#define CHECKSUM_SHA1   ( 1)
#define CHECKSUM_MD5    ( 2)
#define CHECKSUM_RMD160 ( 3)
#define CHECKSUM_TIGER  ( 4)
#define CHECKSUM_GOST   ( 5)

#elif USE_CHECKSUM_OPENSSL
#define CHECKSUMS_LIST  "sha1", "md5", "rmd160"
#define CHECKSUMS_STR   "sha1 md5 rmd160"
#define CHECKSUM_SHA1   ( 0)
#define CHECKSUM_MD5    ( 1)
#define CHECKSUM_RMD160 ( 2)

#elif USE_CHECKSUM_DIET
#define CHECKSUMS_LIST  "md5"
#define CHECKSUMS_STR   "md5"
#define CHECKSUM_MD5    ( 0)

#elif USE_CHECKSUM_BUILT_IN
#define CHECKSUMS_LIST  "md5"
#define CHECKSUMS_STR   "md5"
#define CHECKSUM_MD5    ( 0)

#else
#error "Unknown checksum algorithm library"
#endif

static char *CHECKSUMS[] = { CHECKSUMS_LIST };
#define CHECKSUMS_CNT (sizeof(CHECKSUMS) / sizeof(char *))


/* option processing */
#if USE_CHECKSUM_BY_DEFAULT

#define CHECKSUM_USAGE_c \
    "  -c               ignored. Transmission checksum is activated by\n"   \
    "                   default.\n"

#define CHECKSUM_CASE_c \
    case 'c':           /* checksum */                                      \
        break;

#else

#define CHECKSUM_USAGE_c \
    "  -c               check the whole transmission with checksum.\n"      \
    "                   It is sufficient to use this option only once\n"    \
    "                   (with either netread or netwrite).\n"

#define CHECKSUM_CASE_c \
    case 'c':           /* checksum */                                      \
        if (udp)                                                            \
            usage(argv[0]);                                                 \
        if (params.checksum < 0)                                            \
            params.checksum = 0;                                            \
        break;

#endif

#define OPTION_c
#define OPTION_C
#define CHECKSUM_OPT "cC:"
#define CHECKSUM_USAGE \
    CHECKSUM_USAGE_c                                                        \
    "  -C algorithm     use the specified algorithm for checksum. This\n"   \
    "                   option also implies -c.\n"                          \
    "                   Supported algorithms (the first is default):\n"     \
    "                       " CHECKSUMS_STR " none\n"

#define CHECKSUM_CASE \
    CHECKSUM_CASE_c                                                         \
    case 'C':           /* checksum algorithm */                            \
        if (udp)                                                            \
            usage(argv[0]);                                                 \
        { int index;                                                        \
        for (index = 0; index < CHECKSUMS_CNT; index++) {                   \
            if (strmatch(optarg, CHECKSUMS[index])) {                       \
                params.checksum = index;                                    \
                break;                                                      \
            }                                                               \
        }                                                                   \
        if (index == CHECKSUMS_CNT) {                                       \
            if (strmatch(optarg, "none"))                                   \
                params.checksum = CHECKSUM_NONE;                            \
            else {                                                          \
                params.checksum = 0;                                        \
                print(WARNING, "unknown checksum algorithm ``%s'', "        \
                      "using ``%s''\n", optarg, CHECKSUMS[0]);              \
            }                                                               \
        } }                                                                 \
        break;


/*
 * checksum_init()
 *
 * initialize checksum data structures according to params->checksum
 *
 * return value:
 *      none
 *
 * side effects:
 *      static structure ctx is affected
 */
extern void checksum_init(const struct proto_params *params);

/*
 * checksum_process(buffer, len)
 *
 * process the specified buffer and update the checksum
 *
 * return value:
 *      none
 *
 * side effects:
 *      static structure ctx is affected
 */
extern void checksum_process(const void *buffer, const unsigned int len);

/*
 * checksum_finish()
 *
 * finalize checksum data structures
 *
 * return value:
 *      the computed checksum
 *
 * side effects:
 *      static structure ctx is affected
 *      static string checksum is fill in with the resulting checksum
 */
extern char *checksum_finish(void);

/*
 * checksum_exchange()
 *
 * send the computed checksum through the control connection and receives
 * the checksum computed by the remote party
 *
 * return value:
 *      remotly computed checksum
 *      NULL on failure
 *
 * side effects:
 *      sets static string remote_sum
 */
char *checksum_exchange(void);

/*
 * checksum_validate()
 *
 * compare the computed checksum with the one computed by the remote party
 *
 * return values:
 *      FN_SUCCESS ... ok
 *      FN_FAILURE ... checksums differ
 *
 * side effects:
 *      none
 */
extern int checksum_validate(void);

#endif

