/* $Id: proto.h,v 1.6 2006/02/05 15:19:12 jirka Exp $ */

/*
 * netrw tools
 * Copyright (C) 2002 Jiri Denemark
 *
 * This file is part of netrw.
 *
 * netrw is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef PROTO_H
#define PROTO_H

#include "proto_types.h"
#include "keepalive.h"
#include "checksum.h"


#define PROTO_INIT          "netrw control protocol"

#define PROTO_REQ_PARAMS    "requested params"
#define PROTO_ACC_PARAMS    "accepted params"


#if NEED_PARAMS_ARRAY

#define PARAM_CHECKSUM  0
#define PARAM_KEEPALIVE 1
static char *PARAMS[] = {
    PAR_STR_CHECKSUM,
    PAR_STR_KEEPALIVE
};
#define PARAMS_CNT (sizeof(PARAMS) / sizeof(char *))

#endif


enum party {
    PARTY_NETREAD = 1,
    PARTY_NETWRITE = -1
};


/*
 * params_initialize(params)
 *
 * initialize parameters structure (i.e. set all of the parameters to their
 * default values)
 *
 * return value:
 *      none
 *
 * side effects:
 *      none
 */
extern void params_initialize(struct proto_params *params);

/*
 * params_clear(params)
 *
 * clear parameters structure (i.e. disable all of the parameters) and print
 * what params are being disabled
 *
 * return value:
 *      none
 *
 * side effects:
 *      none
 */
extern void params_clear(struct proto_params *params);

/*
 * proto_initialize(who)
 *
 * start control protocol initialization
 *
 * return value:
 *      FN_SUCCESS ... initialization started
 *      FN_FAILURE ... cannot start initialization
 *
 * side effects:
 *      static variable state is affected
 */
extern int proto_initialize(enum party who);

/*
 * proto_finalize()
 *
 * finalize control protocol
 *
 * return value:
 *      none
 *
 * side effects:
 *      static variable state is affected
 */
extern void proto_finalize(void);

/*
 * proto_complete_init(params)
 *
 * complete control protocol initialization
 *
 * return value:
 *      FN_SUCCESS ... initialization completed
 *      FN_FAILURE ... cannot initialize
 *
 * side effects:
 *      static variable state is affected
 */
extern int proto_complete_init(struct proto_params *params);

/*
 * proto_init_step(params)
 *
 * do one initialization step
 *
 * return value:
 *      FN_SUCCESS
 *      FN_FAILURE
 *
 * side effects:
 *      static variable state is affected 
 */
extern int proto_init_step(struct proto_params *params);

#endif

