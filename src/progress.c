/* $Id: progress.c,v 1.12 2006/01/25 14:33:28 jirka Exp $ */

/*
 * netrw tools
 * Copyright (C) 2002 Jiri Denemark
 *
 * This file is part of netrw.
 *
 * netrw is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#if HAVE_CONFIG_H
#include "config.h"
#endif

#if STDC_HEADERS
#include <stdio.h>
#endif
#if HAVE_UNISTD_H
#include <unistd.h>
#endif
#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif

#include "common.h"
#include "progress.h"


static int bps = 0;
static char unit = 'B';
static unsigned long after = DEFAULT_HASH_AFTER;
static UNSIGNED_LONG_LONG hashes = 0;
static UNSIGNED_LONG_LONG bytes = 0;
static UNSIGNED_LONG_LONG bytes_line = 0;
static struct timeval start;
static struct timeval start_line;


/* static functions */
static void print_bytes(unsigned long long bytes);
static double print_speed(int line);


void progress_speed_bps(void)
{
    bps = 1;
    unit = 'b';
}

void progress_hash_after(unsigned long bytes)
{
    after = (bytes == 0 ? DEFAULT_HASH_AFTER : bytes);
}

void progress_data(unsigned long transferred_bytes)
{
    if (!can_print(NORMAL))
        return;

    if (bytes == 0) {
        gettimeofday(&start, NULL);
        start_line = start;
    }

    bytes += transferred_bytes;
    bytes_line += transferred_bytes;
    while ((bytes / after) >= (hashes + 1)) {
        hashes++;
        print(NORMAL, "#");
        if (hashes % 50 == 0) {
            print(NORMAL, " ");
            print_bytes(hashes * after);
            print(NORMAL, " ");
            print_speed(1);
            print(NORMAL, "\n");

            bytes_line = 0;
            gettimeofday(&start_line, NULL);
        }
        else if (hashes % 10 == 0)
            print(NORMAL, " ");
    }

    fflush(stderr);
}

void progress_summary(void)
{
    unsigned long diff_long;
    double diff;
    unsigned long h, m, s;

    if (!can_print(NORMAL))
        return;

    if (bytes == 0) {
        print(NORMAL, "\nno data transferred\n");
        return;
    }

    print(NORMAL, "\n ");
    print_bytes(bytes);
    print(NORMAL, " (%llu B) transferred at ", bytes);
    diff = print_speed(0);

    diff_long = (unsigned long) diff;
    h = diff_long / (60 * 60);
    diff_long %= 60 * 60;
    m = diff_long / 60;
    s = diff_long %= 60;
    print(NORMAL, " (%lu:%02lu:%02lu.%02lu)\n", h, m, s,
          (unsigned long) ((diff - (unsigned long) diff) * 100));
}


static void print_bytes(unsigned long long bytes)
{
#if HAVE_INT64
    if (bytes >= EXA)
        print(NORMAL, "%7.2f EiB", bytes / (1.0 * EXA));
    else if (bytes >= PETA)
        print(NORMAL, "%7.2f PiB", bytes / (1.0 * PETA));
    else if (bytes >= TERA)
        print(NORMAL, "%7.2f TiB", bytes / (1.0 * TERA));
    else
#endif
         if (bytes >= GIGA)
        print(NORMAL, "%7.2f GiB", bytes / (1.0 * GIGA));
    else if (bytes >= MEGA)
        print(NORMAL, "%7.2f MiB", bytes / (1.0 * MEGA));
    else if (bytes >= KILO)
        print(NORMAL, "%7.2f KiB", bytes / (1.0 * KILO));
    else
        print(NORMAL, "%7d B", (int) bytes);
}

static double print_speed(int line)
{
    double speed;
    double diff;
    struct timeval tv;
    struct timeval now;

    tv = ((line) ? start_line : start);
    gettimeofday(&now, NULL);
    diff = (now.tv_sec + now.tv_usec / 1000000.0)
           - (tv.tv_sec + tv.tv_usec / 1000000.0);
    diff = ((diff == 0) ? 0.001 : diff);

    speed = ((line) ? bytes_line : bytes) / (diff / ((bps) ? 8 : 1));

#if HAVE_INT64
    if (speed > EXA)
        print(NORMAL, "%6.1f Ei%c/s", speed / (1.0 * EXA), unit);
    else if (speed > PETA)
        print(NORMAL, "%6.1f Pi%c/s", speed / (1.0 * PETA), unit);
    else if (speed > TERA)
        print(NORMAL, "%6.1f Ti%c/s", speed / (1.0 * TERA), unit);
    else
#endif
         if (speed > GIGA)
        print(NORMAL, "%6.1f Gi%c/s", speed / (1.0 * GIGA), unit);
    else if (speed > MEGA)
        print(NORMAL, "%6.1f Mi%c/s", speed / (1.0 * MEGA), unit);
    else if (speed > KILO)
        print(NORMAL, "%6.1f Ki%c/s", speed / (1.0 * KILO), unit);
    else
        print(NORMAL, "%6.1f %c/s", speed, unit);

    return diff;
}

