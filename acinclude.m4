m4_define([NW_VERSION], [1.3.2])
m4_define([NW_RELEASE_DATE], [2006/08/15 20:32 UTC])

AC_DEFUN([NW_VER_RELEASE_DATE],
[RELEASE_DATE='NW_RELEASE_DATE'

AC_SUBST([RELEASE_DATE])
])


AC_DEFUN([NW_VER_BUILD_DATE],
[BUILD_DATE=`$DATE_PROGRAM -u '+%Y/%m/%d %H:%M UTC' || echo unknown`

AC_SUBST([BUILD_DATE])
])


AC_DEFUN([NW_VER_BUILDER],
[
if test x$USER = x; then
    BUILDER=`$WHOAMI_PROGRAM || echo somebody`
else
    BUILDER=$USER
fi
if test x$HOST = x; then
    HOST=`$HOSTNAME_PROGRAM`
    if test x$HOST = x; then
        BUILDER=$BUILDER'@'`$UNAME_PROGRAM -n || echo unknown`
    else
        BUILDER=$BUILDER'@'$HOST
    fi
else
    BUILDER=$BUILDER'@'$HOST
fi

AC_SUBST([BUILDER])
])


dnl **************************************************************************
dnl linking with diet libc

AC_DEFUN([NW_DIET_CC],
[AC_CACHE_CHECK([for C compiler],
    [nw_cv_diet_cc],
    [nw_cv_diet_cc_check=$( ( . $srcdir/compiler.sh ) 2>&1 )
    nw_cv_diet_cc=$(echo "$nw_cv_diet_cc_check" |
        fgrep 'NETWRITE_COMPILER:' |
        sed 's/^NETWRITE_COMPILER://'
    )
    if test x$nw_cv_diet_cc = x; then
        AC_MSG_ERROR([cannot detect C compiler: $nw_cv_diet_cc_check])
    fi
    NW_CC="$nw_cv_diet_cc"
    CC="diet $nw_cv_diet_cc -nostdinc"
    ])
])

AC_DEFUN([NW_ENABLE_DIET],
[AC_ARG_ENABLE(diet,
    AC_HELP_STRING([--enable-diet],
        [enable linking against diet libc  @<:@no@:>@]),
    [nw_enable_diet=$enableval],
    [nw_enable_diet=no])

AC_CACHE_CHECK([wether to link against diet libc],
    [nw_cv_enable_diet],
    [nw_cv_enable_diet=$nw_enable_diet])

if test x$nw_cv_enable_diet = xyes; then
    NW_DIET=yes
    NW_DIET_CC
    AC_MSG_NOTICE([diet compiler command... $CC])
    # force --enable-static
    AC_MSG_NOTICE([forcing --enable-static])
    nw_cv_enable_static=yes
    # override --with-checksum
    AC_MSG_NOTICE([overriding --with-checksum with --with-checksum=diet])
    nw_with_checksum_override=diet
else
    NW_DIET=no
fi
])


dnl **************************************************************************
dnl GCC optimizations

AC_DEFUN([NW_ENABLE_GCC_OPTS],
[AC_ARG_ENABLE(gcc-optimizations,
    AC_HELP_STRING([--enable-gcc-optimizations],
        [enable GNU C compiler optimizations @<:@auto@:>@]),
    [nw_enable_gcc_opts=$enableval],
    [nw_enable_gcc_opts=$ac_cv_c_compiler_gnu])

AC_CACHE_CHECK([wether to enable GNU C compiler optimizations],
    [nw_cv_enable_gcc_opts],
    [nw_cv_enable_gcc_opts=$nw_enable_gcc_opts])

if test x$nw_cv_enable_gcc_opts = xyes; then
    nw_gcc_opts_fallback="-O3"
    nw_gcc_opts="-O3 -march=$target_cpu -funroll-loops"
    nw_gcc_opts_CFLAGS=`echo $CFLAGS | sed 's/^-O[[0-9s]]/ &/' | sed 's/-O[[0-9s]]$/& /' | sed 's/ -O[[0-9s]] / /g'`

    AC_CACHE_CHECK([for optimizations],
        [nw_cv_gcc_opts],
        [CFLAGS=`echo "$nw_gcc_opts_CFLAGS $nw_gcc_opts" | sed 's/   */ /g'`
        AC_TRY_COMPILE([], [], [nw_cv_gcc_opts=$nw_gcc_opts], [nw_cv_gcc_opts=$nw_gcc_opts_fallback])])

    CFLAGS=`echo "$nw_cv_gcc_opts $nw_gcc_opts_CFLAGS" | sed 's/   */ /g'`
fi
])


dnl **************************************************************************
dnl Byte order

AC_DEFUN([NW_CHECK_BYTE_ORDER],
[AC_C_BIGENDIAN

if test x$ac_cv_c_bigendian = xyes; then
    nw_byte_order=big-endian
else
    nw_byte_order=little-endian
fi
m4_define([NW_BYTE_ORDER], [$nw_byte_order])
])


dnl **************************************************************************
dnl Use checksum by default

AC_DEFUN([NW_DEFAULT_CHECKSUM],
[AC_ARG_ENABLE(default-checksum,
    AC_HELP_STRING([--enable-default-checksum],
        [enable use of transmission checksum by default @<:@yes@:>@]),
    [nw_enable_def_checksum=$enableval],
    [nw_enable_def_checksum=yes])

AC_CACHE_CHECK([wether to enable checksum by default],
    [nw_cv_enable_def_checksum],
    [nw_cv_enable_def_checksum=$nw_enable_def_checksum])

if test x$nw_cv_enable_def_checksum = xyes; then
    nw_checksum_activation='by default'
    AC_DEFINE(USE_CHECKSUM_BY_DEFAULT, [1],
        [Define to 1 if you want to use transmission checksum by default.])
else
    nw_checksum_activation='on demand'
fi
])


dnl **************************************************************************
dnl Checksum algorithms

AC_DEFUN([NW_WITH_CHECKSUM],
[AC_CHECK_HEADER([mhash.h], [nw_mhash_h=yes], [nw_mhash_h=no])
AC_CHECK_LIB([mhash], [mhash_init], [nw_mhash_l=yes], [nw_mhash_l=no])
AC_CHECK_DECL([MHASH_MD5], [nw_mhash_md5=yes], [nw_mhash_md5=no], [#include <mhash.h>])
AC_CHECK_DECL([MHASH_RIPEMD160], [nw_mhash_rmd=yes], [nw_mhash_rmd=no], [#include <mhash.h>])
AC_CHECK_DECL([MHASH_SHA1], [nw_mhash_sha=yes], [nw_mhash_sha=no], [#include <mhash.h>])
AC_CHECK_DECL([MHASH_SHA256], [nw_mhash_sha256=yes], [nw_mhash_sha256=no], [#include <mhash.h>])
AC_CHECK_DECL([MHASH_TIGER], [nw_mhash_tiger=yes], [nw_mhash_tiger=no], [#include <mhash.h>])
AC_CHECK_DECL([MHASH_GOST], [nw_mhash_gost=yes], [nw_mhash_gost=no], [#include <mhash.h>])
AC_CACHE_CHECK([for mhash usability],
    [nw_cv_mhash],
    [if test x$nw_mhash_h = xyes && \
        test x$nw_mhash_l = xyes && \
        test x$nw_mhash_md5 = xyes && \
        test x$nw_mhash_rmd = xyes && \
        test x$nw_mhash_sha = xyes && \
        test x$nw_mhash_sha256 = xyes && \
        test x$nw_mhash_tiger = xyes && \
        test x$nw_mhash_gost = xyes; then
        nw_cv_mhash=yes
    else
        nw_cv_mhash=no
    fi
    ])

AC_CHECK_HEADER([openssl/evp.h], [nw_openssl_evp_h=yes], [nw_openssl_evp_h=no])
AC_CHECK_LIB([crypto], [EVP_DigestInit], [nw_openssl_evp_l=yes], [nw_openssl_evp_l=no])
AC_CACHE_CHECK([for openssl usability],
    [nw_cv_openssl_evp],
    [if test x$nw_openssl_evp_h = xyes && \
        test x$nw_openssl_evp_l = xyes; then
        nw_cv_openssl_evp=yes
    else
        nw_cv_openssl_evp=no
    fi
    ])

AC_CHECK_HEADER([md5.h], [nw_diet_md5_h=yes], [nw_diet_md5_h=no])
AC_CHECK_FUNC([MD5Update], [nw_diet_md5=yes], [nw_diet_md5=no], [#include <md5.h>])
AC_CACHE_CHECK([for diet libc MD5 implementation],
    [nw_cv_diet_md5],
    [if test x$NW_DIET = xyes && \
        test x$nw_diet_md5_h = xyes && \
        test x$nw_diet_md5 = xyes; then
        nw_cv_diet_md5=yes
    else
        nw_cv_diet_md5=no
    fi
    ])

AC_ARG_WITH(checksum,
    AC_HELP_STRING([--with-checksum=<lib>],
        [select the library the checksum algorithms are taken from. <lib> can
        be `openssl', `mhash', `built-in', `diet', or `auto'.
        @<:@default is auto@:>@]),
    [case $withval in
    built-in|mhash|openssl|diet|auto) nw_with_checksum=$withval ;;
    no) nw_with_checksum=built-in ;;
    *) nw_with_checksum=auto ;;
    esac
    ], [nw_with_checksum=auto])

if test x$nw_with_checksum_override != x; then
    nw_with_checksum=$nw_with_checksum_override
fi

AC_CACHE_CHECK([for checksum algorithm library],
    [nw_cv_with_checksum],
    [nw_checksum_lib=built-in
    
    case $nw_with_checksum in
    mhash)
        if test x$nw_cv_mhash = xyes; then
            nw_checksum_lib=mhash
        fi
        ;;
    openssl)
        if test x$nw_cv_openssl_evp = xyes; then
            nw_checksum_lib=openssl
        fi
        ;;
    diet)
        if test x$nw_cv_diet_md5 = xyes; then
            nw_checksum_lib=diet
        fi
        ;;
    built-in)
        ;;
    auto)
        if test x$nw_cv_diet_md5 = xyes; then
            nw_checksum_lib=diet
        elif test x$nw_cv_openssl_evp = xyes; then
            nw_checksum_lib=openssl
        elif test x$nw_cv_mhash = xyes; then
            nw_checksum_lib=mhash
        fi
    esac

    nw_cv_with_checksum=$nw_checksum_lib])

case $nw_cv_with_checksum in
mhash)
    LIBS="-lmhash $LIBS"
    AC_DEFINE(USE_CHECKSUM_MHASH, [1],
        [Define to 1 if you want checksum functions from mhash.
         Exactly one of `USE_CHECKSUM_MHASH', `USE_CHECKSUM_OPENSSL', 
         `USE_CHECKSUM_BUILT_IN', and `USE_CHECKSUM_DIET' must be defined.])
    
    ;;
openssl)
    LIBS="-lcrypto $LIBS"
    AC_DEFINE(USE_CHECKSUM_OPENSSL, [1],
        [Define to 1 if you want checksum functions from openssl.
         Exactly one of `USE_CHECKSUM_MHASH', `USE_CHECKSUM_OPENSSL', 
         `USE_CHECKSUM_BUILT_IN', and `USE_CHECKSUM_DIET' must be defined.])
    ;;
built-in)
    AC_DEFINE(USE_CHECKSUM_BUILT_IN, [1],
        [Define to 1 if you want built-in checksum functions.
         Exactly one of `USE_CHECKSUM_MHASH', `USE_CHECKSUM_OPENSSL', 
         `USE_CHECKSUM_BUILT_IN', and `USE_CHECKSUM_DIET' must be defined.])
    ;;
diet)
    AC_DEFINE(USE_CHECKSUM_DIET, [1],
        [Define to 1 if you want checksum functions from diet libc.
         Exactly one of `USE_CHECKSUM_MHASH', `USE_CHECKSUM_OPENSSL', 
         `USE_CHECKSUM_BUILT_IN', and `USE_CHECKSUM_DIET' must be defined.])
    ;;
esac

AM_CONDITIONAL(USE_CHECKSUM_MHASH, test x$nw_cv_with_checksum = xmhash)
AM_CONDITIONAL(USE_CHECKSUM_OPENSSL, test x$nw_cv_with_checksum = xopenssl)
AM_CONDITIONAL(USE_CHECKSUM_BUILT_IN, test x$nw_cv_with_checksum = xbuilt-in)
AM_CONDITIONAL(USE_CHECKSUM_DIET, test x$nw_cv_with_checksum = xdiet)
])


dnl **************************************************************************
dnl Static linking

AC_DEFUN([NW_ENABLE_STATIC],
[AC_ARG_ENABLE(static,
    AC_HELP_STRING([--enable-static],
        [enable static linking @<:@no@:>@]),
    [nw_enable_static=$enableval],
    [nw_enable_static=no])

AC_CACHE_CHECK([wether to enable static linking],
    [nw_cv_enable_static],
    [nw_cv_enable_static=$nw_enable_static])

if test x$nw_cv_enable_static = xyes; then
    LDFLAGS=`echo "-static $LDFLAGS" | sed 's/^ *//'`
fi
])


dnl **************************************************************************
dnl int types (uint_8, int_32, uint_32, uint_64)

AC_DEFUN([NW_INT_TYPES],
[
AC_CHECK_SIZEOF(signed char)
AC_CHECK_SIZEOF(unsigned char)
AC_CHECK_SIZEOF(signed short)
AC_CHECK_SIZEOF(unsigned short)
AC_CHECK_SIZEOF(signed int)
AC_CHECK_SIZEOF(unsigned int)
AC_CHECK_SIZEOF(signed long)
AC_CHECK_SIZEOF(unsigned long)
AC_CHECK_SIZEOF(signed long long)
AC_CHECK_SIZEOF(unsigned long long)

AC_CACHE_CHECK([for 32bit unsigned integer type],
    [nw_cv_uint_32],
    [if test x$ac_cv_sizeof_unsigned_short = x4; then
        nw_cv_uint_32="unsigned short"
    elif test x$ac_cv_sizeof_unsigned_int = x4; then
        nw_cv_uint_32="unsigned int"
    elif test x$ac_cv_sizeof_unsigned_long = x4; then
        nw_cv_uint_32="unsigned long"
    else
        nw_cv_uint_32=unknown
    fi
    ])

if test "x$nw_cv_uint_32" != xunknown; then
    AC_DEFINE_UNQUOTED(UINT_32, [$nw_cv_uint_32],
        [Define to 32bit unsigned integer type.])
fi
])


dnl **************************************************************************
dnl configure options

AC_DEFUN([NW_CONF_OPTIONS],
[
NW_CONFIG_COMMAND=
for nw_option in $[]0 "$[]@"; do
    if test "x$NW_CONFIG_COMMAND" != x; then
        NW_CONFIG_COMMAND="$NW_CONFIG_COMMAND    "
    fi
    NW_CONFIG_COMMAND="$NW_CONFIG_COMMAND  '$nw_option'�"
done

NW_CONFIG_COMMAND=`echo "$NW_CONFIG_COMMAND" |
    sed '[s/\(["\\]\)/\\\\\1/g
          s/ *�*$//
          s/ *�/ \\\\\\\\\\\\n/g]'`

AC_SUBST(NW_CONFIG_COMMAND)
])


dnl **************************************************************************
dnl configure info

AC_DEFUN([NW_CONF_INFO],
[
if test "x$prefix" = xNONE; then
    nw_prefix=$ac_default_prefix
else
    nw_prefix=$prefix
fi

NW_CONFIG_INFO=`echo "\
  target      ${target}
  byte order  NW_BYTE_ORDER
  prefix      ${nw_prefix}
  CPPFLAGS    ${CPPFLAGS}
  CFLAGS      ${CFLAGS}
  LIBS        ${LIBS}
  LDFLAGS     ${LDFLAGS}" |
    tr '\n' '�'`
NW_CONFIG_INFO=`echo "$NW_CONFIG_INFO" |
    sed '[s/\(["\\]\)/\\\\\1/g
          s/ *�*$//
          s/ *�/\\\\n/g]'`

AC_SUBST(NW_CONFIG_INFO)
])


dnl **************************************************************************
dnl Summary

AC_DEFUN([NW_SUMMARY],
[AC_MSG_NOTICE([netrw tools summary

                  version   NW_VERSION
             release date   NW_RELEASE_DATE
               build date   ${BUILD_DATE}
                  builder   ${BUILDER}
                build for   ${target}
               byte order   NW_BYTE_ORDER

                   prefix   ${prefix}
      checksum activation   ${nw_checksum_activation}
       checksum functions   ${nw_cv_with_checksum}

                 CPPFLAGS   ${CPPFLAGS}
                   CFLAGS   ${CFLAGS}
                     LIBS   ${LIBS}
                  LDFLAGS   ${LDFLAGS}
])])

